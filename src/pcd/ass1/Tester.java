package pcd.ass1;

public class Tester {

	
	public static void main(String[] args) {
		Universe u = new Universe(2.0, 0.5, 10, 10);
		u.printCurrentParticles();
		System.out.println("----------------------AFTER-------------------------------------");
		u.simulateBehaviourSequential();
		u.printCurrentParticles();
	}
}
