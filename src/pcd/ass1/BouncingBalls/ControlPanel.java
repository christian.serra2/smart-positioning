package pcd.ass1.BouncingBalls;

import javax.swing.*;
import java.awt.event.*;

public class ControlPanel extends JFrame implements ActionListener{
    private JButton buttonPlus;
    private JButton buttonMinus;
    private Context context;
    
    public ControlPanel(Context ctx){
        context = ctx;
        setTitle("Control Panel");
        setSize(250,60);
        setResizable(false);
        this.setLocationRelativeTo(null); //Centra lo schermo
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent ev){
				System.exit(-1);
			}
			public void windowClosed(WindowEvent ev){
				System.exit(-1);
			}
		});

        buttonPlus = new JButton("Start");
        buttonMinus = new JButton("Stop");
        JPanel p = new JPanel();
        p.add(buttonPlus);
        p.add(buttonMinus);
        getContentPane().add(p);
        buttonPlus.addActionListener(this);
        buttonMinus.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent ev){
        Object src = ev.getSource();
        if (src==buttonPlus){
            context.createNewBall();
        } else {
            context.removeBall();
        }
    }
}
