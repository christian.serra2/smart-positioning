package pcd.ass1;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.event.ListSelectionEvent;

public class Universe {
	
	private static final double ALPHA_MIN = 0.1;
	private static final double ALPHA_MAX = 1.0;
	private static final int MASS_MIN = 1;
	private static final int MASS_MAX = 100;
	private static final double Y_MIN = 2.0;
	private static final double Y_MAX = 6.0;
	private static final double X_MIN = 1.0;
	private static final double X_MAX = 5.0;
	private final double k;
	private double time;
	private final double deltaT;
	private final int nSteps;
	private List<Particle> particlesCurrent;
	private List<Particle> particlesNext;
	
	
	public Universe(final double k, final double dt, final int nSteps, final int n) {
		this.k = k;
		this.deltaT = dt;
		this.time = 0.0;
		this.nSteps = nSteps;
		initializeRandomParticles(n, X_MAX, X_MIN, Y_MAX, Y_MIN, MASS_MAX, MASS_MIN, ALPHA_MAX, ALPHA_MIN);
	}
	
	private void initializeRandomParticles(int n, double xMax, double xMin, double yMax, double yMin, 
			int massMax, int massMin, double alphaMax, double alphaMin) {
		Random r = new Random(new Date().getTime());
		particlesCurrent = new ArrayList<>();
		IntStream.range(0, n).forEach((i) -> {
			double alpha = alphaMin + (alphaMax - alphaMin) * r.nextDouble();
			int mass = massMin + r.nextInt(massMax - massMin);
			double xPos = xMin + (xMax - xMin) * r.nextDouble();
			double yPos = yMin + (yMax - yMin) * r.nextDouble();
			particlesCurrent.add(new Particle(alpha, mass, xPos, yPos, 0, 0));
		});
	}
	
	public void simulateBehaviourSequential() {
		for (int i = 1; i <= nSteps; i++) {
			particlesNext = particlesCurrent.stream().map((p) -> {
				double[] force = computeForceOnParticle(p, particlesCurrent);
				double shiftPosX = deltaT * p.getVelocityX();
				double shiftPosY = deltaT * p.getVelocityY();
				double shiftVelX = deltaT * force[0] / p.getMass();
				double shiftVelY = deltaT * force[1] / p.getMass();
				return p.shift(shiftPosX, shiftPosY, shiftVelX, shiftVelY);
			}).collect(Collectors.toList());
			particlesCurrent = new ArrayList<>(particlesNext);
			time += deltaT;
		}
//		t = 0; //Partiamo da tempo = 0
//		for i := 1 to Nsteps  { //Evoluzione temporale di un certo numero di passi Nsteps
//		   for each particle p[i] { //Per ogni particella dell passo i
//			  Compute F[i](t) //Calcola la forza a cui quella particella è soggetta
//			  Update r[i](t)and v[i](t): //Aggiorna la posizione e la velocità della particella i a tempo t.
//				r[i][x] += dt*v[i][x]; //Posizione X della particella = precedente + delta tempo * velocità (Velocità = Spazio / Tempo ==> Spazio = Velocità * Tempo)
//				r[i][y] += dt*v[i][y]; //Posizione Y della particella
//				v[i][x] += dt*f[i][x]/m[i]; //Velocità X della particella = precedente + delta tempo * forza a cui è sottoposta / la massa ( F=m*a , v=a*t ==> F=m*v/t ==> v = F*t/m )
//				v[i][y] += dt*f[i][y]/m[i]; //Velocità Y della particella   
//		   }
//		   t = t + dt //Ad ogni passo completato il tempo viene incrementato del delta tempo passato.
//		}
	}

	//TODO extract pattern strategy
	public void simulateBehaviourParallel(int nThreads) {
		//TODO
	}
	

	private double[] computeForceOnParticle(Particle particle, List<Particle> particlesCurrent){
		double[] force = new double[2];
		for (Particle otherParticle : particlesCurrent) {
			if (otherParticle != particle) {
				double dx = particle.getPositionX() - otherParticle.getPositionX();
				double dy = particle.getPositionY() - otherParticle.getPositionY();
				double d3 = Math.pow(Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)), 3);
				double part = k * particle.getAlfa() * otherParticle.getAlfa() / d3;
				force[0] += part / dx;
				force[1] += part / dy;
				//TODO friction >> velocity
			}
		}
		return force;
//		for each particle j != i { //Per ogni particella che non è quella su cui stiamo calcolando la forza a cui è sottoposta
//		  dx = r[i][x] - r[j][x]; //Differenza di posizione X tra la particella i e la particella j
//		  dy = r[i][y] - r[j][y]; //Differenza di posizione Y tra la particella i e la particella j
//		  d = sqrt(dx*dx+dy*dy); //Distanza tra due punti = SQRT[(x2- x1)^2 + (y2- y1)^2] ==> SQRT(dx^2+dy^2)
//		  d3 = d*d*d; //Il cubo della distanza
//					  //Compare d^3, perché si considera il vettore dalla particella vicina ma modulata ai parametri
//		  f[i][x] += k*alfa[i]*alfa[j]/(d3*(r[i][x]-r[j][x])); //Forza sulla particella in X = precedente + k (costante del sistema) * alfa (proprietà costante relativa a particella)
//															 //							/ distanza al cubo*(differenza di posizione X tra le due particelle) 
//		  f[i][y] += k*alfa[i]*alfa[j]/(d3*(r[i][y]-r[j][y])); //Forza sulla particella in Y
//		}
	}
	
	public double getTime() {
		return time;
	}
	
	public void printCurrentParticles() {
		particlesCurrent.stream().forEach(System.out::println);
	}
}