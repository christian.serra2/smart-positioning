package pcd.ass1;

public class Particle {
	private final double positionX;
	private final double positionY;
	private final double velocityX;
	private final double velocityY;
	
	//Parametri arbitrari
	private final double alfa;
	private final int mass;
	
	
	public Particle(final double alfa, final int mass, final double positionX, final double positionY,
			final double velocityX, final double velocityY){
		this.alfa = alfa;
		this.mass = mass;
		this.positionX = positionX;
		this.positionY = positionY;
		this.velocityX = velocityX;
		this.velocityY = velocityY;
	}
	
	public Particle shift(final double shiftPosX, final double shiftPosY, final double shiftVelX, final double shiftVelY) {
		return new Particle(alfa, mass, positionX + shiftPosX, positionY + shiftPosY,
				velocityX + shiftVelX, velocityY + shiftVelY);
	}
	
	@Override
	public String toString() {
		return "Particle [positionX=" + positionX + ", positionY=" + positionY + ", velocityX=" + velocityX
				+ ", velocityY=" + velocityY + ", alfa=" + alfa + ", mass=" + mass + "]";
	}
	
	public double getPositionX() {
		return positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public double getVelocityX() {
		return velocityX;
	}

	public double getVelocityY() {
		return velocityY;
	}

	public double getAlfa() {
		return alfa;
	}

	public double getMass() {
		return mass;
	}
}
